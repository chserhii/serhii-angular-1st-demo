const Pool = require('pg').Pool
const db = require('./config/db.config')
const pool = new Pool(db.DB_CONFIG)

pool.on('connect', () => {
  console.log('connected to the Database')
})

module.exports = pool
