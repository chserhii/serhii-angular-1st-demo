const serverConfig = require('./config/server.config')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const port = serverConfig.SERVER_PORT
const usersController = require('./controllers/users.controller')
const projectsController = require('./controllers/projects.controller')
const jwt = require('./helpers/jwt')
const errorHandler = require('./helpers/error-handler')
const routes = require('./config/routes')

const app = express()
app.use(cors())
app.options('*', cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(jwt());
app.use(routes.PROJECTS, projectsController);
app.use(routes.USERS, usersController);
app.use(errorHandler);

app.listen(port, () => {
    console.log(`Server listening on port ${port}.`);
});

