--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)

-- Started on 2019-08-06 15:04:40 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13041)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 24972)
-- Name: project_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_user (
    project_id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.project_user OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24959)
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    deleted boolean,
    owner integer NOT NULL
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24957)
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO postgres;

--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 200
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;


--
-- TOC entry 199 (class 1259 OID 24949)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    role character varying(20) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24947)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 198
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 197 (class 1259 OID 24939)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    email character varying(30) NOT NULL,
    password character varying(30) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24937)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2959 (class 0 OID 0)
-- Dependencies: 196
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2804 (class 2604 OID 24962)
-- Name: projects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);


--
-- TOC entry 2803 (class 2604 OID 24952)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2802 (class 2604 OID 24942)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2948 (class 0 OID 24972)
-- Dependencies: 202
-- Data for Name: project_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 2, 1);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 3, 1);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 4, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 5, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 6, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 7, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (1, 8, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 2, 1);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 3, 1);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 4, 1);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 5, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 6, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 7, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 8, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 9, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 10, 2);
INSERT INTO public.project_user (project_id, user_id, role_id) VALUES (2, 11, 2);


--
-- TOC entry 2947 (class 0 OID 24959)
-- Dependencies: 201
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.projects (id, title, description, deleted, owner) VALUES (1, 'ABCDE-q', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor', false, 1);
INSERT INTO public.projects (id, title, description, deleted, owner) VALUES (2, 'QWERTY-z', 'Lorem ipsum dolor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud', false, 1);
INSERT INTO public.projects (id, title, description, deleted, owner) VALUES (3, 'UIJKNM-x', 'Lorem ipsum irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', false, 1);
INSERT INTO public.projects (id, title, description, deleted, owner) VALUES (4, 'Qtyu', 'dtyhnsrba ', NULL, 1);


--
-- TOC entry 2945 (class 0 OID 24949)
-- Dependencies: 199
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles (id, role) VALUES (1, 'maintainer');
INSERT INTO public.roles (id, role) VALUES (2, 'developer');


--
-- TOC entry 2943 (class 0 OID 24939)
-- Dependencies: 197
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (id, name, email, password) VALUES (1, 'Nick M', 'nick-m@gmail.com', 'nick-m');
INSERT INTO public.users (id, name, email, password) VALUES (2, 'Sam P', 'sam-p@gmail.com', 'sam-p');
INSERT INTO public.users (id, name, email, password) VALUES (3, 'Peter D', 'peter-d@gmail.com', 'peter-d');
INSERT INTO public.users (id, name, email, password) VALUES (4, 'John G', 'john-g@gmail.com', 'john-g');
INSERT INTO public.users (id, name, email, password) VALUES (5, 'Andrew K', 'andrew-k@gmail.com', 'andrew-k');
INSERT INTO public.users (id, name, email, password) VALUES (6, 'Oleg R', 'oleg-r@gmail.com', 'oleg-r');
INSERT INTO public.users (id, name, email, password) VALUES (7, 'Samuel P', 'samuel-p@gmail.com', 'samuel-p');
INSERT INTO public.users (id, name, email, password) VALUES (8, 'Juri Z', 'juri-z@gmail.com', 'juri-z');
INSERT INTO public.users (id, name, email, password) VALUES (9, 'Ivan R', 'ivan-r@gmail.com', 'ivan-r');
INSERT INTO public.users (id, name, email, password) VALUES (10, 'Roma S', 'roman-s@gmail.com', 'roman-s');
INSERT INTO public.users (id, name, email, password) VALUES (11, 'Tom Q', 'tom-q@gmail.com', 'tom-q');
INSERT INTO public.users (id, name, email, password) VALUES (12, 'Jack Z', 'jack-z@gmail.com', 'jack-z');
INSERT INTO public.users (id, name, email, password) VALUES (13, 'Vlad S', 'vlad-s@gmail.com', 'vlad-s');
INSERT INTO public.users (id, name, email, password) VALUES (14, 'Dmytro F', 'dmytro-f@gmail.com', 'dmytro-f');
INSERT INTO public.users (id, name, email, password) VALUES (15, 'Taras B', 'taras-b@gmail.com', 'taras-b');
INSERT INTO public.users (id, name, email, password) VALUES (16, 'Oleg Z', 'oleg-z@gmail.com', 'oleg-z');
INSERT INTO public.users (id, name, email, password) VALUES (17, 'Andrij X', 'andrij-x@gmail.com', 'andrij-x');
INSERT INTO public.users (id, name, email, password) VALUES (18, 'Juri T', 'juri-t@gmail.com', 'juri-t');
INSERT INTO public.users (id, name, email, password) VALUES (19, 'Ivan B', 'ivan-b@gmail.com', 'ivan-b');
INSERT INTO public.users (id, name, email, password) VALUES (20, 'Roman W', 'roman-w@gmail.com', 'roman-w');


--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 200
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projects_id_seq', 4, true);


--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 198
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 196
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 20, true);


--
-- TOC entry 2814 (class 2606 OID 24964)
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- TOC entry 2816 (class 2606 OID 24966)
-- Name: projects projects_title_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_title_key UNIQUE (title);


--
-- TOC entry 2810 (class 2606 OID 24954)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2812 (class 2606 OID 24956)
-- Name: roles roles_role_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_role_key UNIQUE (role);


--
-- TOC entry 2806 (class 2606 OID 24946)
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- TOC entry 2808 (class 2606 OID 24944)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2818 (class 2606 OID 24975)
-- Name: project_user project_user_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_user
    ADD CONSTRAINT project_user_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.projects(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2820 (class 2606 OID 24985)
-- Name: project_user project_user_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_user
    ADD CONSTRAINT project_user_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2819 (class 2606 OID 24980)
-- Name: project_user project_user_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_user
    ADD CONSTRAINT project_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2817 (class 2606 OID 24967)
-- Name: projects projects_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_owner_fkey FOREIGN KEY (owner) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2019-08-06 15:04:41 EEST

--
-- PostgreSQL database dump complete
--

