const expressJwt = require('express-jwt')
const authConfig = require('../config/auth.config')
const routes = require('../config/routes')

module.exports = function jwt() {
    const secret = authConfig.AUTH_SECRET
    return expressJwt({ secret }).unless({
        path: [routes.USERS_AUTHENTICATE]
    });
}
