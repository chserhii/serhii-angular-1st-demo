const express = require('express');
const router = express.Router();
const usersService = require('../services/users.service');

router.post('/authenticate', authenticate);
router.get('/', usersService.getUsers);

function authenticate(req, res, next) {
    usersService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

module.exports = router;
