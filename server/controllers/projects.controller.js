const express = require('express');
const router = express.Router();
const projectsService = require('../services/projects.service')

router.get('/:id', projectsService.getProjectById)
router.get('/', projectsService.getProjects)
router.post('/', projectsService.createProject)
router.put('/:id', projectsService.updateProject)
router.delete('/:id', projectsService.deleteProject)
router.get('/:id/users/', projectsService.getUsersByProjectId)

module.exports = router;
