exports.SELECT_USER_VIA_LOGIN_DATA = 'SELECT id, name, email FROM users WHERE email= $1 AND password= $2'
exports.SELECT_USER_BY_ID = 'SELECT id, name, email FROM users WHERE id = $1'
exports.SELECT_USERS = 'SELECT id, name, email FROM users ORDER BY id ASC'
