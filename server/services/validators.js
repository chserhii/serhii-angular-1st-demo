exports.checkId = (id) => {
    const isValidId = id.match(/[0-9]{1,9}/)
    if (!isValidId) response.status(400).send('')
    return isValidId
}

exports.isValidText = (...args) => {
    const ok = args.every(item => item.match(/[A-Za-z0-9.,;'"\s]/gm))
    if (!ok) response.status(400).send('')
    return ok
}
