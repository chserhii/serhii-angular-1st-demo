const authConfig = require('../config/auth.config');
const jwt = require('jsonwebtoken');
const SQL = require('./users-sql-commands')
const pool = require('../pool')
const validators = require('./validators')

async function authenticate({ email, password }) {
  const results = await pool.query(SQL.SELECT_USER_VIA_LOGIN_DATA, [email, password])
  const user = results.rows[0]
  if (user) {
    const payload = { sub: user.id, name: user.name };
    const privateKEY = authConfig.AUTH_SECRET
    const signOptions = { expiresIn: "24h" };
    const token = jwt.sign(payload, privateKEY, signOptions);
    return { token };
  }
}

const getUsers = (request, response) => {
  let sql = SQL.SELECT_USERS, params = []
  const { id } = request.query
  if (id) {
    if (!validators.checkId(id)) return
    sql = SQL.SELECT_USER_BY_ID
    params = [id]
  }
  pool.query(sql, params, (error, results) => {
    pool.end
    if (error) {
      response.status(400).send()
      return
    }
    const data = params.length ? results.rows[0] : results.rows
    response.status(200).json(data)
  })
}

module.exports = {
  authenticate,
  getUsers
};
