exports.SELECT_PROJECTS = 'SELECT * FROM projects ORDER BY id ASC'
exports.SELECT_PROJECT_BY_ID = 'SELECT * FROM projects WHERE id = $1'
exports.INSERT_NEW_PROJECT = 'INSERT INTO projects (title, description, owner) VALUES ($1, $2, $3) RETURNING *'
exports.UPDATE_PROJECT = 'UPDATE projects SET name = $1, email = $2 WHERE id = $3'
exports.DELETE_PROJECT = 'DELETE FROM projects WHERE id = $1'
exports.SELECT_USERS_BY_PROJECT_ID = `SELECT users.name, roles.role, users.email
    FROM project_user
    JOIN users ON users.id = project_user.user_id
    JOIN roles ON roles.id = project_user.role_id
    WHERE project_user.project_id =$1`
