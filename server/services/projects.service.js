const pool = require('../pool')
const SQL = require('./projects.sql.commands')
const validators = require('./validators')


exports.getProjects = (request, response) => {
  pool.query(SQL.SELECT_PROJECTS, (error, results) => {
    pool.end
    if (error) {
      response.status(400).send()
    }
    response.status(200).json(results.rows)
  })
}

exports.getProjectById = (request, response) => {
  const id = request.params.id
  if (!validators.checkId(id)) return

  pool.query(SQL.SELECT_PROJECT_BY_ID, [id], (error, results) => {
    pool.end
    if (error) {
      response.status(400).send();
    } else {
      response.status(200).json(results.rows[0])
    }
  })

}

exports.getUsersByProjectId = (request, response) => {
  const id = request.params.id
  if (!validators.checkId(id)) return

  pool.query(SQL.SELECT_USERS_BY_PROJECT_ID, [id], (error, results) => {
    pool.end
    if (error) {
      response.status(400).send();
      return
    }
    response.status(200).json(results.rows)
  })
}

exports.createProject = (request, response) => {
  const { title, description } = request.body
  if (!validators.isValidText(title, description)) return

  const owner = request.user.sub

  pool.query(SQL.INSERT_NEW_PROJECT, [title, description, owner], (error, result) => {
    pool.end
    if (error) {
      response.status(404).send()
      return
    }
    response.send(`${result.rows[0].id}`)
  })
}

exports.updateProject = (request, response) => {
  const id = request.params.id
  if (!validators.checkId(id) || !validators.isValidText(title, description)) return

  const { title, description, owner } = request.body

  pool.query(SQL.UPDATE_PROJECT, [title, description, owner], (error, results) => {
    pool.end
    if (error) {
      response.status(404).send()
      return
    }
    response.status(200).send()
  })
}

exports.deleteProject = (request, response) => {
  const id = request.params.id
  if (!validators.checkId(id)) return

  pool.query(SQL.DELETE_PROJECT, [id], (error, results) => {
    pool.end
    if (error) {
      response.status(404).send('');
      return
    }
    response.status(200).send()
  })
}
