export class Project {
  id: number;
  title: string;
  description: string;
  deleted?: boolean;
  owner: number;
}
