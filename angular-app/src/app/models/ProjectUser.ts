export class ProjectUser {
  id: number;
  name: string;
  email: string;
  role: string;
}
