import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from '../../services/project.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from '../../models/User';
import { Subscription } from 'rxjs';

@Component({
  selector: 'project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnDestroy {
  newProjectForm: FormGroup;
  submitted = false;
  currentUser: User;
  private subscriptions: Subscription[] = [];


  constructor(
    private formBuilder: FormBuilder,
    private projectService: ProjectService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.subscriptions.push(this.authService.currentUser.subscribe(data => this.currentUser = data));
    this.newProjectForm = this.formBuilder.group({
      id: [0],
      title: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(200)]],
      ownerId: [this.currentUser.id]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.newProjectForm.valid) {
      this.subscriptions.push(this.projectService.addProject(this.newProjectForm.value)
        .subscribe((id) => {
          this.router.navigate(['project-edit', id]);
        }));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe);
  }
}
