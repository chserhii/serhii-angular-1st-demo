import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from '../../models/Project';
import { ProjectService } from '../../services/project.service';
import { Router } from '@angular/router';
import { User } from '../../models/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit, OnDestroy {
  projects: Project[] = [];
  currentUser: User;
  private subscriptions: Subscription[] = [];

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.subscriptions.push(this.authService.currentUser.subscribe(data => this.currentUser = data));
    this.getAllProjects();
  }

  getAllProjects() {
    this.subscriptions.push(this.projectService
      .getProjects()
      .subscribe((data: Project[]) => {
        this.projects = data;
      }));
  }

  createProject(): void {
    this.router.navigate(['project-create']);
  }

  deleteProject(project: Project): void {
    this.subscriptions.push(this.projectService.deleteProject(project.id).subscribe(res => {
      const index = this.projects.findIndex((item) => (item.id === project.id));
      this.projects.splice(index, 1);
    }));
  }

  editProject(project: Project): void {
    this.projectService.setCurrentProject(project);
    this.router.navigate(['project-edit']);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe);
  }
}
