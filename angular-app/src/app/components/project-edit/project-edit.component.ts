import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from '../../services/project.service';
import { UserService } from '../../services/user.service';
import { Project } from '../../models/Project';
import { Subscription } from 'rxjs';
import { ProjectUser } from '../../models/ProjectUser';

@Component({
  selector: 'project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit, OnDestroy {

  project: Project;
  projectUsers: ProjectUser[] = [];
  projectOwner: ProjectUser;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.project = this.projectService.getCurrentProject();

    if (this.project) {
      this.subscriptions.push(this.userService.getUserById(this.project.owner).subscribe(res => {
        this.projectOwner = res as ProjectUser;
      }));
      this.subscriptions.push(this.projectService.getProjectUsers(this.project.id).subscribe(res => {
        this.projectUsers = res as ProjectUser[];
      }));
    } else { { this.router.navigate(['']); } }
  }

  addProjectMember(): void {
    this.router.navigate(['/add-user-to-project']);
  }

  onSearch(searchText: string): void {
    this.subscriptions.push(this.projectService.getProjectUsers(this.project.id).subscribe(res => {
      const data = res as ProjectUser[];
      this.projectUsers =
        (searchText === '') ? data : data.filter(
          item => item.name.toUpperCase().indexOf(searchText.toUpperCase()) !== -1
        );
    }));
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe);
  }
}
