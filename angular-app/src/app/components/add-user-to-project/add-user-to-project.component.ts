import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { Project } from 'src/app/models/Project';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectUser } from 'src/app/models/ProjectUser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'add-user-to-project',
  templateUrl: './add-user-to-project.component.html',
  styleUrls: ['./add-user-to-project.component.css']
})
export class AddUserToProjectComponent implements OnInit, OnDestroy {
  project: Project;
  users: User[];
  projectUsers: ProjectUser[] = [];
  сhecked = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private projectService: ProjectService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.project = this.projectService.getCurrentProject();

    this.subscriptions.push(this.userService.getAllUsers().subscribe(res => {
      this.users = res as User[];
    }, (error) => { console.error(error); }));
  }

  addSelectedUsers(): void {

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe);
  }
}
