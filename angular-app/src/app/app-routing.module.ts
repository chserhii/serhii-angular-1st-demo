import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { ProjectEditComponent } from './components/project-edit/project-edit.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { AddUserToProjectComponent } from './components/add-user-to-project/add-user-to-project.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'project-create',
    component: ProjectCreateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'project-edit',
    component: ProjectEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'add-user-to-project',
    component: AddUserToProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: ProjectsListComponent,
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
