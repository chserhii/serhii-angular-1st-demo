import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { API_URI } from './api-uri';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    const token = sessionStorage.getItem('currentUser');
    let data: any = null;
    if (token) {
      const decodedToken = this.decodeToken(token);
      data = { ...JSON.parse(token), ...decodedToken };
    }
    this.currentUserSubject = new BehaviorSubject<User>(data);
    this.currentUser = this.currentUserSubject.asObservable();
    this.currentUserSubject.next(data);
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  private decodeToken(token: string) {
    const decoded = jwt_decode(token);
    return { id: decoded.sub, name: decoded.name };
  }

  login(email: string, password: string) {
    return this.http.post<any>(API_URI.authenticate, { email, password })
      .pipe(map(user => {
        if (user && user.token) {
          sessionStorage.setItem('currentUser', JSON.stringify(user));
          const decodedToken = this.decodeToken(user.token);
          user = { ...user, ...decodedToken };
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
