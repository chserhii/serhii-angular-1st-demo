import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/Project';
import { API_URI } from './api-uri';

@Injectable({
    providedIn: 'root'
})

export class ProjectService {
    private currentProject: Project;

    constructor(
        private http: HttpClient
    ) { }

    getCurrentProject(): Project {
        return this.currentProject;
    }

    setCurrentProject(newProject: Project): void {
        this.currentProject = newProject;
    }

    getProjects() {
        return this.http.get(API_URI.projects);
    }

    getProject(id: number) {
        return this.http.get(`${API_URI.projects}/${id}`);
    }

    deleteProject(id: number) {
        return this.http.delete(`${API_URI.projects}/${id}`);
    }

    addProject(project: Project) {
        return this.http.post(API_URI.projects, project);
    }

    getProjectUsers(projectId: number) {
        return this.http.get(`${API_URI.projects}/${projectId}${API_URI.projectUsers}`);
    }
}
