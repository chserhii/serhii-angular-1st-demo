import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URI } from './api-uri';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUserById(projectId: number) {
    return this.http.get(`${API_URI.usersById}${projectId}`);
  }

  getAllUsers() {
    return this.http.get(API_URI.users);
  }
}
