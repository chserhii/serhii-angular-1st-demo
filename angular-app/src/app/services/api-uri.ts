const BASE_PATH = 'http://localhost:3000';

export const API_URI = {
    projects: BASE_PATH + '/projects',
    users: BASE_PATH + '/users/',
    usersById: BASE_PATH + '/users/?id=',
    projectUsers: '/users',
    authenticate: BASE_PATH + '/users/authenticate'
};




