import { Component, OnDestroy } from '@angular/core';
import { User } from './models/User';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  currentUser: User;
  private subscription: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.subscription = this.authenticationService.currentUser.subscribe(
      data => this.currentUser = data);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  ngOnDestroy() {
    this.subscription.unsubscripe();
  }
}
